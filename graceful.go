package gss

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"
)

// ___________________________________ ShutdownInfo ___________________________________

// ShutdownInfo is used to add information on the reason of the shutdown
type ShutdownInfo struct {
	Source string
	Reason string
	//Metadata map[string]interface{}
}

// ___________________________________ GracefulManager ___________________________________

// NewGracefulManager create a new GracefulManager
func NewGracefulManager() *GracefulManager {
	return &GracefulManager{}
}

// GracefulManager manage many http.Server to operate in a gracefully mode
type GracefulManager struct {
	servers    []*server
	Logger     *log.Logger
	stopReason atomic.Value
}

// Add a http.Server under a name to easily track it
// No check is done to check already added http.Server
func (s *GracefulManager) Add(name string, srv *http.Server) *GracefulManager {
	if srv == nil {
		return s
	}

	s.servers = append(s.servers, &server{
		name:   name,
		server: srv,
		logger: s.Logger,
	})

	return s
}

// Run starts listening all added http.Server, and waits for a stop request coming from a http.Server stopped
// or an OS signal (syscall.SIGINT, syscall.SIGTERM)
func (s *GracefulManager) Run(ctx context.Context, timeout time.Duration) ShutdownInfo {
	// shutdownChan channel signals the application to finish up
	// len: all added http.Server and the os signal
	var shutdownChan = make(chan ShutdownInfo, len(s.servers)+1)

	// Running the servers in a goroutine so that it won't block the graceful shutdown handling below
	for _, v := range s.servers {
		go v.start(shutdownChan)
	}

	// Run in a goroutine to wait for os signal
	go s.osSignalShutdown(shutdownChan)

	// Wait for shutdwon triggered by context, shutdownChan or stop to trigger all http.Server shutdown
	var shutdown ShutdownInfo
	for shutdown.Source == "" {
		select {
		case shutdown = <-shutdownChan:
		case <-ctx.Done():
			shutdown.Source = "context"
			shutdown.Reason = fmt.Sprintf("%v", ctx.Err())
		default:
			shutdown, _ = s.stopReason.Load().(ShutdownInfo)
		}
		time.Sleep(time.Millisecond)
	}

	// The context is used to inform the server it has some time to finish the request it is currently handling
	var waitStopGroup sync.WaitGroup
	timedCtx, cancelFn := context.WithTimeout(ctx, timeout)
	defer cancelFn()
	for _, v := range s.servers {
		waitStopGroup.Add(1)
		go func(srv *server) {
			defer waitStopGroup.Done()
			srv.stop(timedCtx)
		}(v)
	}
	waitStopGroup.Wait()

	// Return the shutdown reason
	return shutdown
}

// Stop trigger all http.Server shutdown for an external reason
func (s *GracefulManager) Stop(reason string) {
	s.stopReason.Store(ShutdownInfo{
		Source: "external",
		Reason: reason,
	})
}

func (s *GracefulManager) log(format string, v ...interface{}) {
	if s.Logger == nil {
		return
	}
	s.Logger.Printf(format, v...)
}

// osSignalShutdown create a channel to caught os signal (syscall.SIGINT, syscall.SIGTERM)
// and then trigger a shutdown
func (s *GracefulManager) osSignalShutdown(shutdownChan chan ShutdownInfo) {
	// Wait for interrupt signal to trigger gracefully shutdown
	osSignalChan := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	// https://docs.docker.com/engine/reference/commandline/kill/
	// https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-terminating-with-grace
	signal.Notify(osSignalChan, syscall.SIGINT, syscall.SIGTERM)
	sig := <-osSignalChan
	s.log("os signal received:%+v", sig)

	// send message on "finish up" channel to tell the app to gracefully shutdown
	shutdownChan <- ShutdownInfo{
		Source: "os signal",
		Reason: sig.String(),
	}
}

// ___________________________________ server ___________________________________

// NB: as we can not for the moment know if a http.Server is running, shutdown, ...
// we manage it with an internal variable 'running'

type server struct {
	name    string
	server  *http.Server
	running int32
	logger  *log.Logger
}

func (s *server) log(format string, v ...interface{}) {
	if s.logger == nil {
		return
	}
	s.logger.Printf(format, v...)
}

func (s *server) isTLS() bool {
	return s.server.TLSConfig != nil &&
		(len(s.server.TLSConfig.Certificates) > 0 || s.server.TLSConfig.GetCertificate != nil)
}

func (s *server) start(shutdownChan chan ShutdownInfo) {
	// Check if already started
	if atomic.CompareAndSwapInt32(&s.running, 0, 1) == false {
		return
	}

	// Start listen and hang
	s.log("%s: listening on %v", s.name, s.server.Addr)
	var err error
	if s.isTLS() {
		err = s.server.ListenAndServeTLS("", "")
	} else {
		err = s.server.ListenAndServe()
	}

	// Immediately turn running off
	// this informs stop to return, because shutdown s already down, like an external call to http.Server
	// Close or Shutdown method
	atomic.StoreInt32(&s.running, 0)

	// Log error
	if err != http.ErrServerClosed {
		s.log("%s: listening ended: %s", s.name, err)
	}
	s.log("%s: listening ended", s.name)

	// Send message on "finish up" channel to tell the app to gracefully shutdown
	shutdownChan <- ShutdownInfo{
		Source: "server",
		Reason: fmt.Sprintf("%s: listening ended: %s", s.name, err),
	}
}

func (s *server) stop(ctx context.Context) {
	// Check if already stopping
	if atomic.CompareAndSwapInt32(&s.running, 1, 0) == false {
		return
	}

	// Shutdown the server
	s.log("%s: Shutdown called", s.name)
	if err := s.server.Shutdown(ctx); err != nil {
		s.log("%s: Shutdown error:%s", s.name, err)
	}
	s.log("%s: Shutdown ok", s.name)
}
