package gss

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/SylvainDumas/graceful-server-shutdown/internal"
)

const localAddress = "127.0.0.1:0"

func Test_NewGracefulManager(t *testing.T) {
	if manager := NewGracefulManager(); manager == nil {
		t.Error("nil pointer returned")
	}
}

func Test_Graceful_Add(t *testing.T) {
	var manager GracefulManager

	var expectedServersCount int
	if len(manager.servers) != expectedServersCount {
		t.Errorf("servers count expected %v, get %v", expectedServersCount, len(manager.servers))
	}

	if ret := manager.Add("server nil", nil); ret != &manager {
		t.Errorf("bad manager pointer")
	} else if len(manager.servers) != expectedServersCount {
		t.Errorf("servers count expected %v, get %v", expectedServersCount, len(manager.servers))
	}

	expectedServersCount = 1
	var expectedName = "foo"
	var expectedHTTPServer = &http.Server{Handler: http.DefaultServeMux}
	if ret := manager.Add(expectedName, expectedHTTPServer); ret != &manager {
		t.Errorf("bad manager pointer")
	} else if len(manager.servers) != expectedServersCount {
		t.Errorf("servers count expected %v, get %v", expectedServersCount, len(manager.servers))
	} else if srv := manager.servers[0]; srv.name != expectedName {
		t.Errorf("server name expected '%v', get '%v'", expectedName, len(manager.servers))
	} else if srv.server != expectedHTTPServer {
		t.Errorf("server http.Server is not the added one")
	}
}

func Test_Graceful_Run(t *testing.T) {
	var makeLocalServer = func() *http.Server {
		return &http.Server{
			Addr:    localAddress,
			Handler: http.DefaultServeMux,
		}
	}

	var runWith = func(ctxTimeout time.Duration, stopTimeout time.Duration, server *http.Server) ShutdownInfo {
		var manager GracefulManager
		manager.Add("foo", server)

		timeoutCtx, cancelFn := context.WithTimeout(context.Background(), ctxTimeout)
		defer cancelFn()

		go func() {
			time.Sleep(stopTimeout)
			manager.Stop("stopped by stop goroutine")
		}()

		return manager.Run(timeoutCtx, 100*time.Millisecond)
	}

	// Check Run - Stop method
	var expectedSource = "external"
	var expectedReason = "stopped by stop goroutine"
	if err := checkShutdown(runWith(500*time.Millisecond, 250*time.Millisecond, makeLocalServer()), expectedSource, expectedReason); err != nil {
		t.Error(err)
	}

	// Check Run - context stop
	expectedSource = "context"
	expectedReason = "context deadline exceeded"
	if err := checkShutdown(runWith(250*time.Millisecond, 500*time.Millisecond, makeLocalServer()), expectedSource, expectedReason); err != nil {
		t.Error(err)
	}

	// Check Run - server start listen error
	expectedSource = "server"
	expectedReason = "foo: listening ended: http: Server closed"
	localSrv := makeLocalServer()
	go func() {
		time.Sleep(200 * time.Millisecond)
		localSrv.Close()
	}()
	if err := checkShutdown(runWith(500*time.Millisecond, 500*time.Millisecond, localSrv), expectedSource, expectedReason); err != nil {
		t.Error(err)
	}
}

func Test_Graceful_log(t *testing.T) {
	var manager GracefulManager
	// log with no log.Logger do nothing but, do not panic, and add test case covering
	manager.log("foo %v", "bar")

	var logBuffer = bytes.NewBuffer([]byte{})
	manager.Logger = log.New(logBuffer, "", 0)
	manager.log("foo %v", "bar")

	var expectedLog = "foo bar\n"
	if logBuffer.String() != expectedLog {
		t.Errorf("log expected '%v', get '%v'", expectedLog, logBuffer.String())
	}
}

func checkShutdown(shutdown ShutdownInfo, sourceExpected string, reasonExpected string) error {
	if shutdown.Source != sourceExpected {
		return fmt.Errorf("shutdown source expected '%v', get '%v'", sourceExpected, shutdown.Source)
	} else if shutdown.Reason != reasonExpected {
		return fmt.Errorf("shutdown reason expected '%v', get '%v'", reasonExpected, shutdown.Reason)
	}

	return nil
}

type serverTest struct {
	server         *server
	shutdownChan   chan ShutdownInfo
	atomicShutdown atomic.Value
}

func (s *serverTest) checkState(runningExpected int32, sourceExpected string, reasonExpected string) error {
	// Give some time to update values (time to start, stop, goroutines...)
	time.Sleep(time.Millisecond)

	if running := atomic.LoadInt32(&s.server.running); running != runningExpected {
		return fmt.Errorf("server running expected %v, get %v", runningExpected, running)
	}

	shutdown, _ := s.atomicShutdown.Load().(ShutdownInfo)
	return checkShutdown(shutdown, sourceExpected, reasonExpected)
}

func (s *serverTest) start() {
	go s.server.start(s.shutdownChan)
}

func (s *serverTest) stop(ctx context.Context) {
	s.server.stop(ctx)
}

func makeServerTest(name string, srv *http.Server) *serverTest {
	var ret = &serverTest{
		server: &server{
			name:   name,
			server: srv,
		},
		shutdownChan: make(chan ShutdownInfo),
	}

	go func(channel chan ShutdownInfo) {
		ret.atomicShutdown.Store(<-channel)
	}(ret.shutdownChan)

	return ret
}

func Test_server_start_stop(t *testing.T) {
	var srv = makeServerTest("foo", &http.Server{
		Addr:    localAddress,
		Handler: http.DefaultServeMux,
	})

	// Check stop and not running
	srv.stop(nil)
	var runningExpected int32
	if err := srv.checkState(runningExpected, "", ""); err != nil {
		t.Error(err)
	}

	// Start the server
	srv.start()
	// Check server is running
	runningExpected = 1
	if err := srv.checkState(runningExpected, "", ""); err != nil {
		t.Error(err)
	}

	// Start the server a second time
	srv.start()
	// Check server is always running
	if err := srv.checkState(runningExpected, "", ""); err != nil {
		t.Error(err)
	}

	var stopServer = func() {
		timeoutCtx, cancelFn := context.WithTimeout(context.Background(), 100*time.Millisecond)
		defer cancelFn()
		srv.stop(timeoutCtx)
	}

	// Stop the server
	stopServer()
	// Check server is stopped without error
	runningExpected = 0
	if err := srv.checkState(runningExpected, "server", "foo: listening ended: http: Server closed"); err != nil {
		t.Error(err)
	}

	// Stop the server again
	stopServer()
	// Check server is stopped without error
	runningExpected = 0
	if err := srv.checkState(runningExpected, "server", "foo: listening ended: http: Server closed"); err != nil {
		t.Error(err)
	}
}

func Test_server_start_bad_url(t *testing.T) {
	var srv = makeServerTest("foo", &http.Server{
		Addr:    "foo bar",
		Handler: http.DefaultServeMux,
	})

	// Start the server
	srv.start()
	// Give some time to update values (time to start, stop, goroutines...)
	time.Sleep(time.Millisecond)
	if err := srv.checkState(0, "server", "foo: listening ended: listen tcp: address foo bar: missing port in address"); err != nil {
		t.Error(err)
	}
}

func Test_server_start_tls(t *testing.T) {
	cert, err := tls.X509KeyPair(internal.LocalhostCert, internal.LocalhostKey)
	if err != nil {
		t.Fatal(err)
	}

	var srv = makeServerTest("foo", &http.Server{
		Addr:    localAddress,
		Handler: http.DefaultServeMux,
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{cert},
		},
	})

	// Start the server
	srv.start()
	// Give some time to update values (time to start, stop, goroutines...)
	time.Sleep(time.Millisecond)
	if err := srv.checkState(1, "", ""); err != nil {
		t.Error(err)
	}

	// Stop the server
	timeoutCtx, cancelFn := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancelFn()
	srv.stop(timeoutCtx)
	// Give some time to update values (time to start, stop, goroutines...)
	time.Sleep(time.Millisecond)
	if err := srv.checkState(0, "server", "foo: listening ended: http: Server closed"); err != nil {
		t.Error(err)
	}
}

func Test_server_log(t *testing.T) {
	var logBuffer = bytes.NewBuffer([]byte{})
	var srv = server{logger: log.New(logBuffer, "", 0)}
	srv.log("foo %v", "bar")

	var expectedLog = "foo bar\n"
	if logBuffer.String() != expectedLog {
		t.Errorf("log expected '%v', get '%v'", expectedLog, logBuffer.String())
	}
}
