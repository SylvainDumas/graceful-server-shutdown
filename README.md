[![pipeline status](https://gitlab.com/SylvainDumas/graceful-server-shutdown/badges/master/pipeline.svg)](https://gitlab.com/SylvainDumas/graceful-server-shutdown/-/commits/master)
[![coverage report](https://gitlab.com/SylvainDumas/graceful-server-shutdown/badges/master/coverage.svg)](https://gitlab.com/SylvainDumas/graceful-server-shutdown/-/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# Graceful Server Shutdown

# Installation
To install this library, do:
```shell
go get -u gitlab.com/SylvainDumas/graceful-server-shutdown
```
Import it to use it
```go
import gss "gitlab.com/SylvainDumas/graceful-server-shutdown"
```

# Getting Started

## Simple example with net/http
```go
package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	gss "gitlab.com/SylvainDumas/graceful-server-shutdown"
)

func main() {
	// Servers creation
	handler := http.NewServeMux()
	handler.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Welcome net/http Server")
	})

	// Running the server in a graceful shutdown and launch service 'api' listening on port 8080
	gss.NewGracefulManager().Add("api", &http.Server{Addr: ":8080", Handler: handler}).Run(context.Background(), 5*time.Second)
}
```

## Simple example with Gin Gonic
```go
package main

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	gss "gitlab.com/SylvainDumas/graceful-server-shutdown"
)

func main() {
	// Servers creation
	handler := gin.Default()
	handler.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Welcome Gin Server")
	})

	// Running the server in a graceful shutdown and launch service 'api' listening on port 8080
	gss.NewGracefulManager().Add("api", &http.Server{Addr: ":8080", Handler: handler}).Run(context.Background(), 5*time.Second)
}

```

## Create service with a configuration

```go
package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	gss "gitlab.com/SylvainDumas/graceful-server-shutdown"
)

func main() {
	// Read configuration
	var config gss.Config // = readConfiguration()

	// Servers creation
	handler := http.NewServeMux()
	handler.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Welcome net/http Server")
	})

	// Running the server in a graceful shutdown
	gss.NewGracefulManager().Add("api", gss.CreateServer(config, handler)).Run(context.Background(), 5*time.Second)
}
```

## If you want log actions
```go
var manager = gss.GracefulManager{Logger: log.New(os.Stderr, "", log.LstdFlags)}
manager.Add("api", &http.Server{Addr: ":8080", Handler: handler}).Run(context.Background(), 5*time.Second)
```

## Run multiple services
```go
package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	gss "gitlab.com/SylvainDumas/graceful-server-shutdown"
)

func main() {
	// Servers creation
	ginHandler := gin.Default()
	ginHandler.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Welcome Gin Server")
	})
	netHTTPHandler := http.NewServeMux()
	netHTTPHandler.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Welcome net/http Server")
	})

	// Running the server in a graceful shutdown
	gss.NewGracefulManager().
		Add("gin", &http.Server{Addr: ":8080", Handler: ginHandler}).
		Add("net_http", &http.Server{Addr: ":8081", Handler: netHTTPHandler}).
		Run(context.Background(), 5*time.Second)
}
```
