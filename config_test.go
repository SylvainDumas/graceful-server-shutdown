package gss

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"testing"
	"time"
)

func checkServerValues(server *http.Server, addr string, TLSConfig *tls.Config, handler http.Handler, timeouts Timeout) error {
	if server == nil {
		return errors.New("Create server return nil *http.Server")
	}

	if server.Addr != addr {
		return fmt.Errorf("http.Server Addr expected %v, get %v", addr, server.Addr)
	}
	if server.TLSConfig != TLSConfig {
		return fmt.Errorf("http.Server TLSConfig is not the given one")
	}
	if server.Handler != handler {
		return fmt.Errorf("http.Server Handler is not the given one")
	}

	if server.ReadTimeout != timeouts.Read {
		return fmt.Errorf("http.Server ReadTimeout expected %v, get %v", timeouts.Read, server.ReadTimeout)
	}
	if server.ReadHeaderTimeout != timeouts.ReadHeader {
		return fmt.Errorf("http.Server ReadHeaderTimeout expected %v, get %v", timeouts.ReadHeader, server.ReadHeaderTimeout)
	}
	if server.WriteTimeout != timeouts.Write {
		return fmt.Errorf("http.Server WriteTimeout expected %v, get %v", timeouts.Write, server.WriteTimeout)
	}
	if server.IdleTimeout != timeouts.Idle {
		return fmt.Errorf("http.Server IdleTimeout expected %v, get %v", timeouts.Idle, server.IdleTimeout)
	}

	return nil
}

func Test_CreateServer(t *testing.T) {
	// Check Addr building
	if err := checkServerValues(CreateServer(Config{}, nil), "", nil, nil, Timeout{}); err != nil {
		t.Error(err)
	}

	if err := checkServerValues(CreateServer(Config{Host: "192.168.0.1"}, nil), "192.168.0.1", nil, nil, Timeout{}); err != nil {
		t.Error(err)
	}

	if err := checkServerValues(CreateServer(Config{Port: 8085}, nil), ":8085", nil, nil, Timeout{}); err != nil {
		t.Error(err)
	}

	if err := checkServerValues(CreateServer(Config{Host: "192.168.0.1", Port: 8085}, nil), "192.168.0.1:8085", nil, nil, Timeout{}); err != nil {
		t.Error(err)
	}

	// Check TLS config
	var fakeTLSConfig = &tls.Config{}
	if err := checkServerValues(CreateServer(Config{TLS: fakeTLSConfig}, nil), "", fakeTLSConfig, nil, Timeout{}); err != nil {
		t.Error(err)
	}

	// Check handler
	if err := checkServerValues(CreateServer(Config{}, http.DefaultServeMux), "", nil, http.DefaultServeMux, Timeout{}); err != nil {
		t.Error(err)
	}

	// Check timeouts
	var expectedTimeouts = Timeout{
		Read:       time.Millisecond,
		ReadHeader: time.Second,
		Write:      time.Minute,
		Idle:       time.Hour,
	}
	if err := checkServerValues(CreateServer(Config{Timeout: expectedTimeouts}, nil), "", nil, nil, expectedTimeouts); err != nil {
		t.Error(err)
	}
}
