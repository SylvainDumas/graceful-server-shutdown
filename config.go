package gss

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"time"
)

// Timeout define the http.Server timeouts
type Timeout struct {
	Read       time.Duration
	ReadHeader time.Duration
	Write      time.Duration
	Idle       time.Duration
}

// Config define a base configuration for an http.Server
type Config struct {
	Host    string
	Port    uint16
	TLS     *tls.Config
	Timeout Timeout
}

// CreateServer create an http.Server using the base configuration Config
func CreateServer(config Config, handler http.Handler) *http.Server {
	var addr = config.Host
	if config.Port != 0 {
		addr += fmt.Sprintf(":%v", config.Port)
	}
	return &http.Server{
		Addr:      addr,
		TLSConfig: config.TLS,
		Handler:   handler,
		// Good practice to set timeouts to avoid Slowloris attacks.
		ReadTimeout:       config.Timeout.Read,
		ReadHeaderTimeout: config.Timeout.ReadHeader,
		WriteTimeout:      config.Timeout.Write,
		IdleTimeout:       config.Timeout.Idle,
	}
}
